---
# doc :
#   - https://github.com/aquasecurity/trivy#readme
#   - https://aquasecurity.github.io/trivy/v0.17.2/integrations/gitlab-ci/
#
# This job use Trivy for scanning container image build by the pipeline
#
# - One full scan is printed on job log.
# - One Junit report is printed on Pipeline tests
#   Errors are also printed on Merge Request "Test summary".
# - One Container Scanning report upload
# The Job fail on severity in TRIVY_SEVERITY_FAIL
#
# TRIVY_VERSION: defalt 0.52.0
# TRIVY_SEVERITY_FAIL: List of the severity for failing.
#                      Values can be : "LOW,MEDIUM,HIGH,CRITICAL"
#                      Default : "HIGH,CRITICAL"
# IMAGE_NAME: is the name of the image builded

container:security:
  extends:  .base_tpl
  stage:    test
  image:
    name: aquasec/trivy:${TRIVY_VERSION}
    entrypoint: [""]
  variables:
    GIT_STRATEGY: none
    TRIVY_VERSION: "0.52.0"
    TRIVY_USERNAME: "${CI_REGISTRY_USER}"
    TRIVY_PASSWORD: "${CI_REGISTRY_PASSWORD}"
    TRIVY_AUTH_URL: "${CI_REGISTRY}"
    TRIVY_SEVERITY_FAIL: "HIGH,CRITICAL"
    TRIVY_CACHE_DIR: .trivycache/
    TRIVY_IGNORE_UNFIXED: "true"
    TRIVY_NO_PROGRESS:    "true"
    TRIVY_INSECURE:       "true"
    TRIVY_TEMPLATE_DIRECTORY: "/contrib"
    TRIVY_DB_REPOSITORY:  "public.ecr.aws/aquasecurity/trivy-db"
    TRIVY_JAVA_DB_REPOSITORY: "public.ecr.aws/aquasecurity/trivy-java-db"
    CS_OUTPUT:   "gl-container-scanning-report.json"
    JUNIT_OUTPUT: "junit-report.xml"
  script:
    - trivy --version
    # cache cleanup is needed when scanning images with the same tags, it does not remove the database
    - time trivy image --clear-cache
    # update vulnerabilities db
    - time trivy --download-db-only --no-progress
    - |- # Namespace
        container_namespace="${CI_PROJECT_PATH}/${tpl_tmp_image_namespaces}"
    - |- # Image name
        if [[ -n "${IMAGE_NAME}" ]]; then
          container_image_name="/${IMAGE_NAME}"
        else
          container_image_name=""
        fi
    - |- # Tags
        container_tag="tmp-branch-${CI_COMMIT_REF_NAME//\//_}-$(echo ${CI_COMMIT_SHA} | cut -c1-8)"
    - |- # Full image name
        full_image_name=${container_namespace}${container_image_name}
        echo "⚙️ Test image: "
        echo "  🏭 registry : ${CI_REGISTRY}"
        echo "  📦     name : ${full_image_name}"
        echo "       🏷️ tag : ${container_tag}"
        image_to_scan="${CI_REGISTRY}/${full_image_name}:${container_tag}"
    # Prints full report
    - time trivy --no-progress "${image_to_scan}"
    # Builds report for GitLab Container Scanning
    # https://docs.gitlab.com/ee/user/application_security/container_scanning/
    - time trivy --no-progress --format template --template "@${TRIVY_TEMPLATE_DIRECTORY}/gitlab.tpl"
        --output "${CS_OUTPUT}" "${image_to_scan}"
    # Builds report for GitLab Junit
    # https://docs.gitlab.com/ee/ci/unit_test_reports.html
    - time trivy --no-progress --format template --template "@${TRIVY_TEMPLATE_DIRECTORY}/junit.tpl"
        --output ${JUNIT_OUTPUT} "${image_to_scan}"
    # Fails on vulnerabilities
    - time trivy --no-progress --exit-code 1 --severity ${TRIVY_SEVERITY_FAIL} "${image_to_scan}"
  cache:
    key: trivy-cache
    paths:
      - .trivycache
  artifacts:
    when: always
    expire_in: 15 days
    reports:
      container_scanning: gl-container-scanning-report.json
