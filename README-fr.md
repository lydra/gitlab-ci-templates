# Gitlab-ci templates

[🇬🇧 English version](README.md)

Simplifiez vos pipelines GitLab ! 🥰
Une collection utile de *templates* et de *librairies* pour gitlab-ci.

Les fichiers de ce dépôt peuvent être inclut dans vos `.gitlab-ci.yml`.

Pour utiliser ce dépôt vous devez connaître les notions suivantes :
- [include](https://docs.gitlab.com/ce/ci/yaml/#include)
- [extends](https://docs.gitlab.com/ce/ci/yaml/#extends)

[Le guide de référence de GitLab-CI se trouve ici.](https://docs.gitlab.com/ee/ci/yaml/README.html)


## Organisation du dépôt

```bash
├── doc
├── templates
├── gci-templates
└── test
```


### doc

Ce répertoire contient les documentations du projet.


### gci-templates

Ce répertoire contient tous les templates qui peuvent être utilisés dans les piplines.


### gci-templates

Ce répertoire contient tous les anciens templates qui pouvais être utilisés dans les piplines.
⚠️ Ce repertoire est déprécié.


#### activate-dind.yml

Ce fichier permet d’activer docker in docker.
Tous les jobs de ce dépôt utilisent docker.


#### stages.yml

Ce fichier contient la définition des stages et des templates de bases utilisés dans les jobs.


#### job

Ce répertoire contient les templates de job.
Chaque job est classé par type ou technologie dans un sous-répertoire.


### test

Ce répertoire contient les ressources nécessaires aux tests.


## Utilisation

Pour utiliser les _templates_ de CI de ce dépôt il faut les inclure dans le fichier `.gitlab-ci` de votre dépôt.

Chaque fichier contient un, ou plusieurs, jobs classés en fonction de leur thématique.

Pour avoir des exemples d'implémentation vous avez [ce groupe qui contient des dépôts d'exemples](https://xc001xg9.siege.intra.groupe-casino.fr/ci-cd/exemples).


### `stages.yml`

Ce fichier contient la liste des étapes de base de la CI.
Il faut l'inclure en premier quand on veut utiliser les _templates_ de ce dépôt.
Il contient aussi les modèles de base des jobs.

Vous pouvez utiliser ces modèles pour concevoir vos propres jobs.

```yaml
node:test:
  extends: .base_tpl
  stage: test
  script:
    - make test

```


#### Les étapes

##### info

Cette étape sert à informer les utilisateurs de la CI.
On peut y mettre un peu ce que l'on veut.
Les mainteneurs de ce dépôt peuvent s'en servir pour informer les utilisateurs des dépôts tiers.

Vous pouvez dans votre CI vous en servir par exemple pour :
- Afficher le tag de la _release_ à créer.
- Afficher des informations à destination de l'utilisateur de la CI.

Vous avez à disposition 3 jobs modèles qui utilisent tous "Docker in Docker".


###### `.info`

Ce job affiche un message et se termine en succès.


###### `.warning`

Ce job affiche un message, se termine en warning et la CI se poursuit.


###### `.error`

Ce job affiche un message, se termine en warning et la CI s’arrête.


###### Usage

Job d'**information** dans l'étape d'info.

```yaml
Mon information:
  extends: .info
  variables:
    message: "Ceci est un message d'information"
```

Job de **warning** dans une autre étape.

```yaml
Mon warning dans test:
  extends: .warning
  stage: test
  variables:
    message: "Ceci est un message de warning dans les tests"
```


##### Lint

Cette étape contient tous les jobs de lint.
C'est là que l'on fait les analyses statiques qui n'ont pas besoin de dépendances applicatives.


##### Build

Cette étape contient tous les jobs de build de l'application.
Elle génère souvent des fichiers caches.


##### Package

Cette étape contient tous les jobs de packaging des applications.
C'est souvent la création des images de conteneurs temporaires.


##### Test

Cette étape contient tous les jobs de tests.
C'est là que l'on fait les analyses statiques et dynamiques qui ont besoin des dépendances applicatives.


##### Delivery

Cette étape contient tous les jobs d’empaquetage des applications :
- Création des  _releases_.
- Création des images de conteneurs et _push_ dans le registre du dépôt, et dans le registre de production.
- Mise à disposition des  _releases_ ou paquet dans un dépôt d'artefact.
 
Les jobs de cette étape n'existent que lorsqu'un tag est poussé sur le dépôt GitLab.


[🔎 Pour approfondir.](doc/fr/livraison.md)


##### Deploy

Cette étape contient tous les jobs de déploiement des applications :
- Envoi du code sur un serveur distant.
 
Les jobs de cette étape n'existent que lorsqu'un tag est poussé sur le dépôt GitLab et ils sont **à déclenchement manuel**.


#### Les modèles de base des jobs

Ce dépôt tire partie des fonctionnalités d'[include](https://docs.gitlab.com/ce/ci/yaml/#include) et d'[extends](https://docs.gitlab.com/ce/ci/yaml/#extends) de GitLab-CI pour rendre le code modulaire.
Il est important de les connaître pour mètre en place les _templates_ et pouvoir faire de l'héritage.

Les jobs modèle commencent tous par un point (`.`) et ne sont pas destinés à être utilisés tels quels.
Pour GitLab ce sont des [jobs cachés](https://docs.gitlab.com/ee/ci/yaml/README.html#hide-jobs) et ils ne sont pas exécutés dans la CI.
Par contre n'importe quel job peut en hériter grâce au mot clef `extends`.

##### `.base_tpl`

C'est le job de base dont hérite tous les autres jobs.
La variable `working_directory` contient le nom du répertoire du job.
La valeur par défaut est `$CI_BUILDS_DIR` qui est le répertoire dans le conteneur ou l'on veut exécuter le job.
Cela peut être utile si vous voulez exécuter le même job dans plusieurs répertoires.

Par exemple pour builder deux images docker donc les Dokerfiles sont `front/Dokerfile` et `back/Dockerfile` il faudra faire :

```yaml
# .gitlab-ci.yml

include:
  - project: 'ci-cd/gci-tpl'
    ref: master
    file:
      - '/templates/stages.yml'
      - '/templates/job/container/lint.yml'

container:delivery:front:
  extends: "container:delivery"
  variables:
  working_directory: "front"
  
container:delivery:back:
  extends: "container:delivery"
  variables:
  working_directory: "back"
```


#### Surcharge de job

Dans votre fichier de CI quand vous importez un fichier de job celui-ci va s’exécuter tel qu'il a été défini.
Si vous voulez modifier son comportement alors vous pouvez le surcharger en redéfinissant les parties qui vous intéressent.
La surcharge détourne la fonctionnalité de [fusion de gitlab-ci](https://docs.gitlab.com/ce/ci/yaml/#merge-details).
C'est pour ça qu'il est important de bien comprendre que GitLab est capable de fusionner des hachages mais pas des tableaux.
La surcharge fonctionne car dans GitLab c'est toujours les clés du dernier membre qui remplace ce qui est défini avant.

Par exemple les `variables:` s’additionnent là ou les `tags:` ou le `script:` se remplacent.


##### Exemple d'utilisation

Je veux utiliser `npm ci` à la place de `yarn build` pour construire mon application node.
Il faut donc que je surcharge le script du job `node:build`.

Mon `.gitlab-ci.yml` devra être comme ça :

```yaml
# .gitlab-ci.yml
include:
  - project: 'ci-cd/gci-tpl'
    ref: master
    file:
      - '/templates/stages.yml'
      - '/templates/job/node/build.yml'

node:build:
  script:
    - npm ci
```

Seule la partie script sera modifiée, toute le reste de la définition du job `nodebuild` sera comme dans le fichier `https://gitlab.com/lydra/gitlab-ci-templates/-/raw/master/templates/job/node/build.yml`

Ce qui donne comme résultat :

```yaml
node:build:
  image: node:14.15.4-slim
  stage: build
  variables:
    working_directory: "$CI_PROJECT_DIR"
  cache:
    policy: push
    key: "node-$CI_COMMIT_REF_SLUG"
    paths:
      - node_modules
  before_script:
    - cd $working_directory
  script:
    - npm ci
```

## Registre de conteneurs

Si vous utilisez le registre de conteneurs, avec le job `container:build:` pensez à purger les images temporaires, taguées `tmp`.

![](doc/img/container_cleanup_policy.png)

Source : https://docs.gitlab.com/ee/user/packages/container_registry/#delete-images-by-using-a-cleanup-policy


## Pour aller plus loin
- [La référence de `.gitlab-ci.yml`](https://docs.gitlab.com/ce/ci/yaml).

---
Création graphique par [Freepik](https://www.freepik.com) sur [www.flaticon.com](https://www.flaticon.com) sous licence [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0)
