# Livraison

## Vocabulaire

- **Livraison :** une livraison est la mise à disposition d'un artefact, comme une archive compressée (tgz, jar ou war) ou image de conteneur. Cet artefact est destiné à être installé sur les environnements d’exécution. Quand elle est automatisée on parle de [livraison continue](https://fr.wikipedia.org/wiki/Livraison_continue) (CD : [Continuous Delivery](https://en.wikipedia.org/wiki/Continuous_delivery))
- **Déploiement :** un déploiement est l'installation environnement d’exécution. On prend souvent le résultat de la livraison pour l’installé sur les serveurs ou lancer des images dans Kubernetes. Quand il est automatisé on parle de [déploiement continu](https://fr.wikipedia.org/wiki/D%C3%A9ploiement_continu) (CD : [Continuous Deployement](https://en.wikipedia.org/wiki/Continuous_deployment))

## Description de la Livraison standard

Ce sont les évènements git qui déclenchent la livraison. En, fonction de l’évènement déclencheur les jobs ne font pas les mêmes actions.

### Artefact

Un artefact est un ensemble de fichier mis à disposition pour une livraison. Un artefact peut-être inclus dans une _release_.
Dans notre exemple le résultat de la livraison est une image de conteneur car c’est le cas standard mais cela peu être un autre type d’artefact comme une archive jar.

### Branche de fonctionnalité

A chaque `git push` sur GitLab dans une branche de fonctionnalité le pipeline génère une image temporaire dans le registre de conteneur du projet aillant comme nom :
`mon_projet/tmp:tmp-branch-<nom_de_branche>-<hash_court>`

### Fusion dans la branche principale

A chaque _merge_ suite à une _Merge Request_ dans GitLab dans la branche principale _master_ ou _main_ (à partir de [GitLab 14](https://docs.gitlab.com/ee/user/project/repository/branches/default.html)) le pipeline génère une image temporaire dans le registre du projet aillant comme nom :
`mon_projet/tmp:tmp-branch-<nom_de_branche>-<hash_court>`

### Création d’un tag

La création d’un tag est souvent un des prémices à la création d’une _release_. Un tag est régulièrement un numéro de version.
À chaque _merge_ suite à une _Merge Request_ dans GitLab dans la branche principale _master_ ou _main_ (à partir de [GitLab 14](https://docs.gitlab.com/ee/user/project/repository/branches/default.html)) le pipeline génère une image dans le registre de conteneur :
- du projet aillant comme nom : `mon_projet:<tag>`
- du dépôt des images de livraison aillant comme nom : `mon_projet:<tag>`

Voir [les normes de nommage d’images de conteurs](https://gitpr01.siege.intra.groupe-casino.fr/developpement/gitlab-casino/-/blob/master/Images_docker_casino.md).

Ce comportement est modifiable, voir [le job container:delivery](../templates/job/container/delivery.yml) pour plus de précisions.
